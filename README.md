# SSHFP and OpenSSH

## Pre-requisites

- Server to test SSHing to; key access set up
- DNSSEC enabled for the domain and zone
- SSHFP record properly set

## Running

```bash
docker-compose up -d
docker ps
docker exec -it <CONTAINER_ID> /bin/bash
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/id_rsa
# Enter passphrase if applicable
ssh -v -oVerifyHostKeyDNS=yes -i ~/.ssh/id_rsa username@example.com
```

> **DISCLAIMER**
> You can also duplicate all the steps in the `Dockerfile` on your local machine.
> Keep in mind that you may break something and these instructions come with _No Warranty™_.
> Also, keep in mind that the steps in the `Dockerfile` are specific to Ubuntu and may not be compatible with the CLI options on Mac, BSD, or Windows.

## Stopping

```bash
docker-compose down
```

## Reference Links

### Compiling OpenSSH
- https://www.tecmint.com/install-openssh-server-from-source-in-linux/
- https://www.openssh.com/portable.html#http
- https://www.nlnetlabs.nl/projects/ldns/download/

### Information about DNSSEC
- https://www.cloudflare.com/dns/dnssec/how-dnssec-works/
- https://en.wikipedia.org/wiki/Domain_Name_System_Security_Extensions

### Setting up DNSSEC
- https://cloud.google.com/dns/docs/dnssec
- https://cloud.google.com/dns/docs/dnssec-advanced
- https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/domain-configure-dnssec.html

### Testing DNSSEC
- Client Test: https://dnssec.vs.uni-due.de/
- https://dnssec-analyzer.verisignlabs.com/
- http://dnsviz.net/
- https://en.internet.nl/
- https://www.internetsociety.org/deploy360/dnssec/tools/

### Information about SSHFP
- https://en.wikipedia.org/wiki/SSHFP_record
- https://tools.ietf.org/html/rfc4255

### Setting up SSHFP
- https://cloud.google.com/dns/docs/dnssec-advanced#sshfp
- https://jpmens.net/2012/07/27/verifyhostkeydns-yessssss/
- https://www.internetsociety.org/blog/2012/07/how-to-hack-openssh-to-add-dnssec-validation/
- https://ayesh.me/sshfp-verification

### Useful SSH docs
- http://manpages.ubuntu.com/manpages/bionic/en/man1/ssh.1.html
- http://manpages.ubuntu.com/manpages/bionic/en/man1/ssh-keygen.1.html
- http://manpages.ubuntu.com/manpages/bionic/en/man1/ssh-keyscan.1.html

## SSHFP Notes

### SSH Client Options
For your OpenSSH client to take advantage of DNSSEC and SSHFP, you will need two things.

1. OpenSSH compiled with `--with-ldns` enabled (as demonstrated in the Dockerfile)
2. `VerifyHostKeyDNS` set to `yes`

```
Host *
  VerifyHostKeyDNS yes
```

Or, you can pass it as an option when connecting:

```bash
ssh -oVerifyHostKeyDNS=yes #...
```

So a full SSH command could look something like this:

```bash
ssh -v -oVerifyHostKeyDNS=yes -i ~/.ssh/id_rsa username@example.com
```

Some people were recommending also setting `RES_OPTIONS=edns0` but I didn't see any difference in my testing with or without it.

### Notes on Behavior

One interesting thing I noted was that when you are SSHing with `VerifyHostKeyDNS=yes` is that it will not add the fingerprint to your `known_hosts` file. Keep this in mind if you have any scripting that depends on its presence there. You may want to do an `ssh-keyscan` deliberately before any scripts that can't take advantage of SSH's DNSSEC SSHFP verification.

### Testing the SSHFP Record

```bash
dig example.com SSHFP
```

Result:

```bash
; <<>> DiG 9.10.6 <<>> example.com SSHFP
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 25341
;; flags: qr rd ra ad; QUERY: 1, ANSWER: 8, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1452
;; QUESTION SECTION:
;example.com. IN  SSHFP

;; ANSWER SECTION:
example.com. 60 IN  SSHFP 2 2 B47197296AD8AC899B08696A4C6F414C0CA168E32ADAF30B830D1354 AA954B5B
example.com. 60 IN  SSHFP 3 1 A705678AD779496D992F177BF667DA0E731106A6
example.com. 60 IN  SSHFP 3 2 33799C5B988A85E7F6C516C7581ACF1BBF72D4C6A6D559B391CDAFB0 413960FC
example.com. 60 IN  SSHFP 4 1 ACF3FB02680FB99E15DD83656EFAE4F6B5471EC6
example.com. 60 IN  SSHFP 4 2 10E5860E25DA65EFAAAF6FDB28588ED5A2C7245FD65F2482D824CB84 C54E083F
example.com. 60 IN  SSHFP 1 1 E8DE1677102DD7759FB4AD147A3CA36E10EE6829
example.com. 60 IN  SSHFP 1 2 1CC88170667A32C6DED6179DBDA63450B1FBF53BECE58F015F9FAD98 8F958790
example.com. 60 IN  SSHFP 2 1 11E25DAD837F6D167F43C227C6FE49D37CB747AB

;; Query time: 46 msec
;; SERVER: 1.0.0.1#53(1.0.0.1)
;; WHEN: Sun Mar 03 13:53:31 MST 2019
;; MSG SIZE  rcvd: 372
```

### Generating SSHFP for a server: On the Host

```bash
# Set the DNS_NAME to whatever
export DNS_NAME=example.com

ssh-keygen -r $DNS_NAME -f /etc/ssh/ssh_host_ecdsa_key.pub
ssh-keygen -r $DNS_NAME -f /etc/ssh/ssh_host_dsa_key.pub
ssh-keygen -r $DNS_NAME -f /etc/ssh/ssh_host_ed25519_key.pub
ssh-keygen -r $DNS_NAME -f /etc/ssh/ssh_host_rsa_key.pub
```

Example output for one record

```bash
username@instance-1:~$ export DNS_NAME=example.com
username@instance-1:~$ ssh-keygen -r $DNS_NAME -f /etc/ssh/ssh_host_rsa_key.pub
example.com IN SSHFP 1 1 7f466a85bfe6cd21236aedd6260fcf20803f9e2d
example.com IN SSHFP 1 2 f1fccb7e6422f6e503b7a2febd646d415f051b845779f331b2bd687961704cec
```

We will copy the values starting after `example.com IN SSHFP` and get:

```
1 1 7f466a85bfe6cd21236aedd6260fcf20803f9e2d
1 2 f1fccb7e6422f6e503b7a2febd646d415f051b845779f331b2bd687961704cec
```

These will become the values in our `SSHFP` DNS record for the `example.com` domain.

The `1 1` and `1 2` at the beginning of the record value tell you the algorithm and fingerprint type respectively.

So, `1 2` would be `DSA` algorithm and `SHA-256` fingerprint type.

A full `SSHFP` DNS record value would look like this:

```
1 1 7f466a85bfe6cd21236aedd6260fcf20803f9e2d
1 2 f1fccb7e6422f6e503b7a2febd646d415f051b845779f331b2bd687961704cec
2 1 af1b1f305f7ef398ff92965a078ddbcc65fa69a8
2 2 052f1ecf335bd7beba0cf4f85b4781bfef290767228e8c8685f9570ac119eea4
3 1 2953b861391b9bd1e947a3d831526a20e6974fb1
3 2 29197f9745de0c2759b75c2a1d02e3626cd69bd2ab88d5de9618fe5c1cf13633
4 1 2d76c5c9ff2d7993b65f1352cbbc205ee9f5b7bb
4 2 4e1836eb10676584f6932c6e5e04c99f67794a5de7d2e1dfafdcc47d3da06f98
```

#### Algorithm
1. RSA
2. DSA
3. ECDSA
4. Ed25519

#### Fingerprint type
1. SHA-1
2. SHA-256

### Generating SSHFP for a server: From ssh-keygen

> **WARNING** I was not able to get this method to work. `ssh-keygen` was giving me different fingerprints from what the on host file based fingerprint generation was giving me. I may have been doing something wrong, so let me know if you are aware of how to make this method work.

Run this command to generate the SSHFP formatted fingerprint records from the `known_hosts` file on your machine.

```bash
ssh-keygen -r example.com
```

You will get something like this:

```
example.com IN SSHFP 1 1 e8de1677102dd7759fb4ad147a3ca36e10ee6829
example.com IN SSHFP 1 2 1cc88170667a32c6ded6179dbda63450b1fbf53bece58f015f9fad988f958790
example.com IN SSHFP 2 1 11e25dad837f6d167f43c227c6fe49d37cb747ab
example.com IN SSHFP 2 2 b47197296ad8ac899b08696a4c6f414c0ca168e32adaf30b830d1354aa954b5b
example.com IN SSHFP 3 1 a705678ad779496d992f177bf667da0e731106a6
example.com IN SSHFP 3 2 33799c5b988a85e7f6c516c7581acf1bbf72d4c6a6d559b391cdafb0413960fc
example.com IN SSHFP 4 1 acf3fb02680fb99e15dd83656efae4f6b5471ec6
example.com IN SSHFP 4 2 10e5860e25da65efaaaf6fdb28588ed5a2c7245fd65f2482d824cb84c54e083f
```

You can ignore the `example.com IN SSHFP` portion and use this format for the `SSHFP` DNS record value:

```
1 1 e8de1677102dd7759fb4ad147a3ca36e10ee6829
1 2 1cc88170667a32c6ded6179dbda63450b1fbf53bece58f015f9fad988f958790
2 1 11e25dad837f6d167f43c227c6fe49d37cb747ab
2 2 b47197296ad8ac899b08696a4c6f414c0ca168e32adaf30b830d1354aa954b5b
3 1 a705678ad779496d992f177bf667da0e731106a6
3 2 33799c5b988a85e7f6c516c7581acf1bbf72d4c6a6d559b391cdafb0413960fc
4 1 acf3fb02680fb99e15dd83656efae4f6b5471ec6
4 2 10e5860e25da65efaaaf6fdb28588ed5a2c7245fd65f2482d824cb84c54e083f
```

Make sure to include the newline characters, or enter each line in a field if that's how your DNS zone host works (like Google DNS).

#### ssh-keyscan to get fingerprints

```bash
ssh-keyscan -H example.com
```

The default is to fetch "rsa", "ecdsa", and "ed25519" keys.

If you want to fetch all key types, then run:

```bash
ssh-keyscan -t rsa,dsa,ecdsa,ed25519 -H example.com
```

To set the results in your `known_hosts` file, for example, run:

```bash
ssh-keyscan -t rsa,dsa,ecdsa,ed25519 -H example.com >> ~/.ssh/known_hosts
```

> **Note**
> The `-H` option will hash all hostnames and addresses in the output.
> Hashed names may be used normally by ssh and sshd, but they do not reveal identifying information should the file's contents be disclosed.
> – [man page](http://manpages.ubuntu.com/manpages/bionic/en/man1/ssh-keyscan.1.html)
