FROM ubuntu

## References
# https://www.tecmint.com/install-openssh-server-from-source-in-linux/
#

# Make sure you have the build dependencies you'll need
# Install libldns-dev package to get the headers so we can build properly,
## otherwise you'll get an error during the OpenSSH configure step
RUN apt-get update -qq \
  && DEBIAN_FRONTEND=noninteractive apt-get install -y \
  build-essential \
  wget \
  zlib1g-dev \
  libssl-dev \
  libldns-dev \
  && rm -rf /var/lib/apt/lists/*

## Set up sshd user and group
RUN mkdir /var/lib/sshd
RUN chmod -R 700 /var/lib/sshd/
RUN chown -R root:sys /var/lib/sshd/
RUN useradd -r -U -d /var/lib/sshd/ -c "sshd privsep" -s /bin/false sshd

# Download the OpenSSH source
## For a full list and your choice of mirror, go here: https://www.openssh.com/portable.html#http
RUN wget -c https://cdn.openbsd.org/pub/OpenBSD/OpenSSH/portable/openssh-7.9p1.tar.gz
RUN tar -xzf openssh-7.9p1.tar.gz
WORKDIR openssh-7.9p1/

# Configure OpenSSH with LDNS (add any of your own favorite flags as well)
## You can see all configuration options with:
### ./configure -h
RUN ./configure --with-ldns

# Build OpenSSH
RUN make && make install

# Verify version
RUN ssh -V

# Verify configuration
RUN sshd -T

# Set up a sane SSH config file
RUN mkdir ~/.ssh/ && touch ~/.ssh/config
RUN echo "\
Host *\n\
  VerifyHostKeyDNS yes\n\
  IdentityFile ~/.ssh/id_rsa\n\
" >> ~/.ssh/config

# Cheap trick to keep the container alive so we can exec into it
CMD tail -f /dev/null
